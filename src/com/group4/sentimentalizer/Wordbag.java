package com.group4.sentimentalizer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.regex.Pattern;

public class Wordbag {
	protected HashMap<String, Integer> wordbag;
	protected int total_words = 0;
	public double wordbagProbability = 0;
	
	public Wordbag() {
		wordbag = new HashMap<String, Integer>();
	}
	
	public void processFile(File file) {
		BufferedReader reader = null;
		final String REGEXP = "[\\W&&[^']]+";
		Pattern p = Pattern.compile(REGEXP);
		
		try {
			reader = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String line;
		try {
			while ((line = reader.readLine()) != null) {
				line = line.trim();
				if (line.isEmpty())
					continue;
				final String[] words = p.split(line);
				String word;
				for (int i = 0; i < words.length; ++i) {
					word = words[i];
					word.trim();
					if (word.isEmpty()) {
						continue;
					}
					word = word.toLowerCase();
					if (wordbag.containsKey(word)) {
						wordbag.put(word, wordbag.get(word)+1);
					} else {
						wordbag.put(word, new Integer(1));
					}
					++total_words;
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			reader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public HashMap<String, Integer> getMap(){
	
		return wordbag;
		
	}

	public void printWordbag() {
		
		
		for (String key : wordbag.keySet()) {
			System.out.println(key +": "+wordbag.get(key)+" ("+(float)wordbag.get(key)/total_words+")");
		}
		
		System.out.println("=== top words");
		for (String key : wordbag.keySet()) {
			if (wordbag.get(key) > 500) {
				System.out.println(key+": "+wordbag.get(key)+" ("+(float)wordbag.get(key)/total_words+")");
			}
		}
		
	}
	
	public double getProbability(String word) {
		final float m = 1;
		final float n = 2;
		final Integer qty = wordbag.get(word);
		if (qty != null) {
			return (
					(double)qty.intValue()+(1.0/total_words)
					)/(
					(double)total_words + 1
					);
		} else {
			return (m/(total_words+n));
		}
		//return 0;
	}
}
