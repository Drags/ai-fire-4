package com.group4.sentimentalizer.bayes.external;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Pattern;

import net.sf.javaml.classification.Classifier;
import net.sf.javaml.classification.KNearestNeighbors;
import net.sf.javaml.core.Dataset;
import net.sf.javaml.core.DefaultDataset;
import net.sf.javaml.core.Instance;

public class KNN {

	final static String[] categories = { "books", "dvd", "camera", "health", "music", "software" };

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("KNN starting");
		
		Dataset dataset = new DefaultDataset();
		final int fileLimit = 50;
		final int K = 13;
		System.out.println("K = "+K);
		for (int i = 0; i < categories.length; ++i) {
			ArrayList<File> fileList = new ArrayList<File>();
			fileList.add(new File("punctuation/" + categories[i] + "/"));
			System.out.println("=== category " + categories[i]);
			int fileCount = 0;
			while (!fileList.isEmpty()) {
				File file = fileList.get(fileList.size() - 1);
				fileList.remove(fileList.size() - 1);
				if (file.isDirectory()) {
					File[] objectsInDir = file.listFiles();
					for (File objectInDir : objectsInDir) {
						fileList.add(objectInDir);
					}
				} else {
					++fileCount;
					SparseNamedInstance sni = createSNI(file);
					sni.setClassValue(categories[i]);
					dataset.add(sni);
					if (fileCount >= fileLimit) {
						fileList.clear();
					}
				}
			}
		}
		System.out.println("starting classification");
		int correct = 0, wrong = 0;

		/* Classify all instances and check with the correct class values */
		Classifier knn = new KNearestNeighbors(K);
		knn.buildClassifier(dataset);
		for (Instance inst : dataset) {
			if ((correct + wrong) % 10 == 0) {
				System.out.println(((correct+wrong)/10)+"0 processed out of "+dataset.size() +" :: correct: "+correct+" wrong: "+wrong);
			}
		    Object predictedClassValue = knn.classify(inst);

		    Object realClassValue = inst.classValue();

		    if (predictedClassValue.equals(realClassValue)) {

		        correct++;
		    }
		    else {
		        wrong++;
		    }

		}
		System.out.println(correct+" "+wrong);
	}

	private static SparseNamedInstance createSNI(File file) {
		SparseNamedInstance result = new SparseNamedInstance();
		BufferedReader reader = null;
		final String REGEXP = "[\\W&&[^']]+";
		Pattern p = Pattern.compile(REGEXP);
		
		try {
			reader = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String line;
		try {
			while ((line = reader.readLine()) != null) {
				line = line.trim();
				if (line.isEmpty())
					continue;
				final String[] words = p.split(line);
				String word;
				for (int i = 0; i < words.length; ++i) {
					word = words[i];
					word.trim();
					if (word.isEmpty()) {
						continue;
					}
					word = word.toLowerCase();
					result.put(word, result.get(word)+1);
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			reader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}// TODO Auto-generated method stub

		return result;
	}
}
