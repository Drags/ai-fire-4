package com.group4.sentimentalizer.bayes.external;

import java.util.HashMap;
import java.util.Map;

import net.sf.javaml.core.SparseInstance;

public class SparseNamedInstance extends SparseInstance {

	static protected HashMap<String, Integer> legend = new HashMap<String,Integer>();
	static protected int lastInteger = 1;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2073388908107109104L;
	
	public Double put(String key, Double value) {
		Integer integerKey = legend.get(key);
		if (integerKey == null) {
			integerKey = new Integer(lastInteger++);
			legend.put(key, integerKey);
		}
		
		return super.put(integerKey,  value);
	}
	
	public Double get(String key) {
		Integer integerKey = legend.get(key);
		if (integerKey == null) {
			return new Double(0);
		}
		return super.get(legend.get(key));
	}
}
