package com.group4.sentimentalizer.bayes;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Pattern;

import com.group4.sentimentalizer.Wordbag;

public class Sentimentalizer {

	final static String[] mainCategories = { "books", "dvd", "camera", "health", "music", "software" };
	final static String[] categories = { "pos", "neg" };
	private static final int N_FOLD = 10;

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		for (int i = 0; i < mainCategories.length; ++i) {
			System.out.println("IN DOMAIN: " + mainCategories[i]);
			int tp[] = new int[N_FOLD];
			int tn[] = new int[N_FOLD];
			int fp[] = new int[N_FOLD];
			int fn[] = new int[N_FOLD];
			for (int N = 0; N < N_FOLD; ++N) {
				System.out.println("starting " + N + " fold");
				final Wordbag[] wordbags = new Wordbag[categories.length];
				for (int r = 0; r < wordbags.length; ++r) {
					wordbags[r] = new Wordbag();
				}
				double[] probability = new double[wordbags.length];
				int total_count = 0;
				System.out.println("Starting learning");
				ArrayList<File> fileList = new ArrayList<File>();
				for (int l = 0; l < categories.length; l++) {
					fileList.add(new File("punctuation/" + mainCategories[i] + "/" + categories[l] + "/"));
					System.out.println("=== learning from " + categories[l]);
					while (!fileList.isEmpty()) {
						File file = fileList.get(fileList.size() - 1);
						fileList.remove(fileList.size() - 1);
						if (file.isDirectory()) {
							File[] objectsInDir = file.listFiles();
							ArrayList<File> filesInDir = new ArrayList<File>();
							ArrayList<File> subDirs = new ArrayList<File>();
							for (int k = 0; k < objectsInDir.length; ++k) {
								if (objectsInDir[k].isDirectory()) {
									subDirs.add(objectsInDir[k]);
								} else {
									filesInDir.add(objectsInDir[k]);
								}
							}
							final int testStart = filesInDir.size() / N_FOLD * N;
							final int testEnd = testStart + filesInDir.size() / N_FOLD;
							for (int j = 0; j < filesInDir.size(); ++j) {
								if (j < testStart || j >= testEnd) {
									fileList.add(filesInDir.get(j));
								}
							}
							for (int j = 0; j < subDirs.size(); ++j) {
								fileList.add(subDirs.get(j));
							}
						} else {
							++probability[l];
							++total_count;
							wordbags[l].processFile(file);
						}
					}
				}

				for (int r = 0; r < wordbags.length; ++r) {
					probability[r] /= total_count;
					wordbags[r].wordbagProbability = probability[r];
				}

				System.out.println("Starting verification");
				fileList = new ArrayList<File>();

				for (int l = 0; l < categories.length; l++) {
					fileList.add(new File("punctuation/" + mainCategories[i] + "/" + categories[l] + "/"));
					while (!fileList.isEmpty()) {
						File file = fileList.get(fileList.size() - 1);
						fileList.remove(fileList.size() - 1);
						if (file.isDirectory()) {
							File[] objectsInDir = file.listFiles();
							ArrayList<File> filesInDir = new ArrayList<File>();
							ArrayList<File> subDirs = new ArrayList<File>();
							for (int k = 0; k < objectsInDir.length; ++k) {
								if (objectsInDir[k].isDirectory()) {
									subDirs.add(objectsInDir[k]);
								} else {
									filesInDir.add(objectsInDir[k]);
								}
							}
							final int testStart = filesInDir.size() / N_FOLD * N;
							final int testEnd = testStart + filesInDir.size() / N_FOLD;
							for (int j = testStart; j < testEnd; ++j) {
								fileList.add(filesInDir.get(j));
							}
							for (int j = 0; j < subDirs.size(); ++j) {
								fileList.add(subDirs.get(j));
							}
						} else {
							double[] results = calculateProbabilities(file, wordbags);
							double max = Double.MIN_VALUE;
							int max_i = 0;
							for (int j = 0; j < results.length; ++j) {
								if (max < results[j]) {
									max = results[j];
									max_i = j;
								}
							}
							if (categories[l] == "neg") {
								if (l == max_i) {
									++tn[N];
								} else {
									++fp[N];
								}
							} else {
								if (l == max_i) {
									++tp[N];
								} else {
									++fn[N];
								}
							}
						}
					}
				}			

			}
			float gacc = 0;
			float gpre = 0;
			float grec = 0;
			float gf1 = 0;
			float negativeAccuracy = 0;
			for (int j = 0; j < N_FOLD; ++j) {
				//System.out.println("Statistics of " + (j + 1) + " fold:");
				float accuracy = (float) (tp[j] + tn[j]) / (float) (tp[j] + tn[j] + fp[j] + fn[j]);
				gacc += accuracy;
				float precision = (float) tp[j] / (float) (tp[j] + fp[j]);
				gpre += precision;
				float recall = (float) tp[j] / (float) (tp[j] + fn[j]);
				grec += recall;
				float f1score = 2 * precision * recall / (precision + recall);
				gf1 += f1score;
				negativeAccuracy += (float)tn[j]/(float)(tn[j]+fp[j]);
				// System.out.println("Acc: " + accuracy + " Pre: " +
				// precision
				// + " Rec: " + recall + " F1: " + f1score);
			}
			gpre /= N_FOLD;
			gacc /= N_FOLD;
			grec /= N_FOLD;
			gf1 /= N_FOLD;
			negativeAccuracy /= N_FOLD;
			System.out.println("Average for category "+mainCategories[i]+":");
			System.out.println("Acc: " + gacc + " Pre: " + gpre + " Rec: " + grec + " NAcc: "+negativeAccuracy+" F1: " + gf1);
			// here goes calculation of stats per category
		}
	}

	static public double[] calculateProbabilities(File file, Wordbag[] wordbags) {
		double calc_probability[] = new double[wordbags.length];
		for (int i = 0; i < wordbags.length; ++i) {
			calc_probability[i] = 1;
		}

		BufferedReader reader = null;
		final String REGEXP = "[\\W&&[^']]+";
		Pattern p = Pattern.compile(REGEXP);

		try {
			reader = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String line;
		try {
			while ((line = reader.readLine()) != null) {
				line = line.trim();
				if (line.isEmpty())
					continue;
				final String[] words = p.split(line);
				String word;
				for (int i = 0; i < words.length; ++i) {
					word = words[i];
					word.trim();
					if (word.isEmpty()) {
						continue;
					}
					word = word.toLowerCase();
					double length = 0;
					for (int j = 0; j < wordbags.length; ++j) {
						calc_probability[j] *= wordbags[j].getProbability(word);
						length += calc_probability[j] * calc_probability[j];
					}
					length = Math.sqrt(length);
					for (int j = 0; j < wordbags.length; ++j) {
						calc_probability[j] /= length;
					}

				}
			}
		} catch (IOException e) {
			// id
		}

		for (int i = 0; i < wordbags.length; ++i) {
			calc_probability[i] *= wordbags[i].wordbagProbability;
		}

		return calc_probability;
	}

}
