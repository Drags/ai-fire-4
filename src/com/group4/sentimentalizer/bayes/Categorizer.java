package com.group4.sentimentalizer.bayes;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Pattern;

import com.group4.sentimentalizer.Wordbag;

public class Categorizer {

	final static String[] categories = { "books", "dvd", "camera", "health", "music", "software" };

	private static final int N_FOLD = 10;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		final float[] averages = new float[categories.length];
		for (int N = 0; N < N_FOLD; ++N) {
			System.out.println("starting" + N+" fold");
			
			final Wordbag[] wordbags = new Wordbag[categories.length];
			for (int i = 0; i < wordbags.length; ++i) {
				wordbags[i] = new Wordbag();
			}

			double[] probability = new double[wordbags.length];
			int total_count = 0;
			System.out.println("Starting learning");
			for (int i = 0; i < categories.length; ++i) {
				ArrayList<File> fileList = new ArrayList<File>();
				fileList.add(new File("punctuation/" + categories[i] + "/"));
				System.out.println("=== category " + categories[i]);
				while (!fileList.isEmpty()) {
					File file = fileList.get(fileList.size() - 1);
					fileList.remove(fileList.size() - 1);
					if (file.isDirectory()) {
						File[] objectsInDir = file.listFiles();
						ArrayList<File> filesInDir = new ArrayList<File>();
						ArrayList<File> subDirs = new ArrayList<File>();
						for (int k = 0; k < objectsInDir.length; ++k) {
							if (objectsInDir[k].isDirectory()) {
								subDirs.add(objectsInDir[k]);
							} else {
								filesInDir.add(objectsInDir[k]);
							}
						}
						final int testStart = filesInDir.size() / N_FOLD * N;
						final int testEnd = testStart + filesInDir.size() / N_FOLD;
						for (int j = 0; j < filesInDir.size(); ++j) {
							if (j < testStart || j >= testEnd) {
								fileList.add(filesInDir.get(j));
							}
						}
						for (int j = 0; j < subDirs.size(); ++j) {
							fileList.add(subDirs.get(j));
						}
					} else {
						++probability[i];
						++total_count;
						wordbags[i].processFile(file);
					}
				}

			}

			for (int i = 0; i < wordbags.length; ++i) {
				probability[i] /= total_count;
				wordbags[i].wordbagProbability = probability[i];
			}

			System.out.println("Starting verification");
			for (int i = 0; i < categories.length; ++i) {
				int fileCounter = 0;
				int correctCategories = 0;
				ArrayList<File> fileList = new ArrayList<File>();
				fileList.add(new File("punctuation/" + categories[i] + "/"));
				System.out.println("=== category " + categories[i]);
				while (!fileList.isEmpty()) {
					File file = fileList.get(fileList.size() - 1);
					fileList.remove(fileList.size() - 1);
					if (file.isDirectory()) {
						File[] objectsInDir = file.listFiles();
						ArrayList<File> filesInDir = new ArrayList<File>();
						ArrayList<File> subDirs = new ArrayList<File>();
						for (int k = 0; k < objectsInDir.length; ++k) {
							if (objectsInDir[k].isDirectory()) {
								subDirs.add(objectsInDir[k]);
							} else {
								filesInDir.add(objectsInDir[k]);
							}
						}
						final int testStart = filesInDir.size() / N_FOLD * N;
						final int testEnd = testStart + filesInDir.size() / N_FOLD;
						for (int j = testStart; j < testEnd; ++j) {
							fileList.add(filesInDir.get(j));
						}
						for (int j = 0; j < subDirs.size(); ++j) {
							fileList.add(subDirs.get(j));
						}
					} else {
						++fileCounter;
						double[] results = calculateProbabilities(file, wordbags);
						double max = Double.MIN_VALUE;
						int max_i = 0;
						for (int j = 0; j < results.length; ++j) {
							if (max < results[j]) {
								max = results[j];
								max_i = j;
							}
						}

						if (i == max_i) {
							++correctCategories;
						} else {
							System.out.println(categories[max_i]+" assigned: "+file.getAbsolutePath());
						}
					}
				}
				final float ratio = correctCategories / (float) fileCounter;
				averages[i] += ratio;
				System.out.println(ratio);
			}
		}
		System.out.println("Average after 10-fold:");
		for (int i = 0; i < averages.length ;++i) {
			System.out.println(categories[i]+" "+averages[i]);
		}
	}

	static public double[] calculateProbabilities(File file, Wordbag[] wordbags) {
		double calc_probability[] = new double[wordbags.length];
		for (int i = 0; i < wordbags.length; ++i) {
			calc_probability[i] = 1;
		}

		BufferedReader reader = null;
		final String REGEXP = "[\\W&&[^']]+";
		Pattern p = Pattern.compile(REGEXP);

		try {
			reader = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String line;
		try {
			while ((line = reader.readLine()) != null) {
				line = line.trim();
				if (line.isEmpty())
					continue;
				final String[] words = p.split(line);
				String word;
				for (int i = 0; i < words.length; ++i) {
					word = words[i];
					word.trim();
					if (word.isEmpty()) {
						continue;
					}
					word = word.toLowerCase();
					double length = 0;
					for (int j = 0; j < wordbags.length; ++j) {
						calc_probability[j] *= wordbags[j].getProbability(word);
						length += calc_probability[j] * calc_probability[j];
					}
					length = Math.sqrt(length);
					for (int j = 0; j < wordbags.length; ++j) {
						calc_probability[j] /= length;
					}

				}
			}
		} catch (IOException e) {
			// id
		}

		for (int i = 0; i < wordbags.length; ++i) {
			calc_probability[i] *= wordbags[i].wordbagProbability;
		}

		return calc_probability;
	}

}