package com.group4.sentimentalizer.bayes;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Pattern;

import com.group4.sentimentalizer.Wordbag;

public class Outimentalizer {

	final static String[] mainCategories = { "books", "dvd", "camera", "health", "music", "software" };
	final static String[] ratings = { "pos", "neg" };

	final static int N_FOLD = 10;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Outimentalizer starting...");
		for (String catName : mainCategories) {
			System.out.println("Learning from category: " + catName);
			final Wordbag[] wordbags = new Wordbag[ratings.length];
			for (int i = 0; i < wordbags.length; ++i) {
				wordbags[i] = new Wordbag();
			}

			double[] probability = new double[wordbags.length];
			int total_count = 0;
			System.out.println("Starting learning");
			for (int i = 0; i < ratings.length; ++i) {
				ArrayList<File> fileList = new ArrayList<File>();
				fileList.add(new File("punctuation/" + catName + "/" + ratings[i] + "/"));
				System.out.println("=== " + catName + " " + ratings[i]);
				while (!fileList.isEmpty()) {
					File file = fileList.get(fileList.size() - 1);
					fileList.remove(fileList.size() - 1);
					if (file.isDirectory()) {
						File[] objectsInDir = file.listFiles();
						for (int k = 0; k < objectsInDir.length; ++k) {
							fileList.add(objectsInDir[k]);
						}
					} else {
						++probability[i];
						++total_count;
						wordbags[i].processFile(file);
					}
				}
			}

			for (int i = 0; i < wordbags.length; ++i) {
				probability[i] /= total_count;
				wordbags[i].wordbagProbability = probability[i];
			}

			System.out.println("Starting verification");
			for (String verifiedWithCat : mainCategories) {
				System.out.println("=== verified with "+verifiedWithCat);
				for (int i = 0; i < ratings.length; ++i) {
					int fileCounter = 0;
					int correctRatings = 0;
					ArrayList<File> fileList = new ArrayList<File>();

					fileList.add(new File("punctuation/" + verifiedWithCat + "/" + ratings[i] + "/"));
					while (!fileList.isEmpty()) {
						File file = fileList.get(fileList.size() - 1);
						fileList.remove(fileList.size() - 1);
						if (file.isDirectory()) {
							File[] objectsInDir = file.listFiles();
							for (int k = 0; k < objectsInDir.length; ++k) {
								fileList.add(objectsInDir[k]);
							}
						} else {
							++fileCounter;
							double[] results = calculateProbabilities(file, wordbags);
							double max = Double.MIN_VALUE;
							int max_i = 0;
							for (int j = 0; j < results.length; ++j) {
								if (max < results[j]) {
									max = results[j];
									max_i = j;
								}
							}

							if (i == max_i) {
								++correctRatings;
							} else {
								for (int j = 0; j < results.length; ++j) {
									// System.out.print(results[j]+" ");
								}
								// System.out.println();
							}
						}
					}
					final float ratio = correctRatings / (float) fileCounter;
					System.out.print(" " + ratings[i]+" = "+ ratio);
				}
				System.out.println();
			}
		}

	}

	static public double[] calculateProbabilities(File file, Wordbag[] wordbags) {
		double calc_probability[] = new double[wordbags.length];
		for (int i = 0; i < wordbags.length; ++i) {
			calc_probability[i] = 1;
		}

		BufferedReader reader = null;
		final String REGEXP = "[\\W&&[^']]+";
		Pattern p = Pattern.compile(REGEXP);

		try {
			reader = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String line;
		try {
			while ((line = reader.readLine()) != null) {
				line = line.trim();
				if (line.isEmpty())
					continue;
				final String[] words = p.split(line);
				String word;
				for (int i = 0; i < words.length; ++i) {
					word = words[i];
					word.trim();
					if (word.isEmpty()) {
						continue;
					}
					word = word.toLowerCase();
					double length = 0;
					for (int j = 0; j < wordbags.length; ++j) {
						calc_probability[j] *= wordbags[j].getProbability(word);
						length += calc_probability[j] * calc_probability[j];
					}
					length = Math.sqrt(length);
					for (int j = 0; j < wordbags.length; ++j) {
						calc_probability[j] /= length;
					}

				}
			}
		} catch (IOException e) {
			// id
		}

		for (int i = 0; i < wordbags.length; ++i) {
			calc_probability[i] *= wordbags[i].wordbagProbability;
		}

		return calc_probability;
	}

}
