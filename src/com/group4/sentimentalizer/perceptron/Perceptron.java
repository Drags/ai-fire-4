package com.group4.sentimentalizer.perceptron;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.HashMap;

import com.group4.sentimentalizer.Wordbag;

//I am Perceptron

/**
 * Perceptron classifier
 * 
 * 
 * @author johan & simon
 */
public class Perceptron {	
	HashMap<String, Integer> classifierDictionary;
	HashMap<String, Integer> documentDictionary;
	HashMap<String,Integer> w;
	float bias = 0;
	float learningRate = 0.5f;
	long lastTime = System.nanoTime();
	String category;
	
	
	public Perceptron(Wordbag wordbag, String category){
		classifierDictionary = new HashMap<String, Integer>();
		classifierDictionary.putAll(wordbag.getMap());
		this.clearValues(classifierDictionary);
		
		documentDictionary = new HashMap<String, Integer>();
		documentDictionary.putAll(classifierDictionary);
		
		w = new HashMap<String, Integer>();
		
		this.category = category;
		
		
	}
	
	public void train(HashMap<HashMap<String, Integer>, String> trainMap){
		System.out.print("	Started training. ");
		w.putAll(classifierDictionary);

		System.out.print("	Training iteration: ");
		for(int i = 0; i < 5; i++){
			System.out.print(i + "..");
			//Iterate over the entire set of documents.
			for(Entry<HashMap<String, Integer>, String> trainEntry : trainMap.entrySet()){
				//Put current document into the complete dictionary.
				HashMap<String, Integer> documentMap = documentDictionary;

				documentMap.putAll(trainEntry.getKey());

				double result = classify(trainEntry.getKey());
				boolean expected = trainEntry.getValue().equals(category);

				//If maths then maths
				if((result > 0) == expected){
					//Do nothing
				} else if ((result < 0) && expected){ //If result false and expected true
					//Decrease bias with learning rate
					bias -= learningRate;
					//Add values of documents to w
					for(Entry<String, Integer> KVPair : w.entrySet()){
						w.put(KVPair.getKey(), KVPair.getValue() + documentMap.get(KVPair.getKey()));
					}
				} else {//If result true and expected false
					//Increase bias with learning rate
					bias += learningRate;
					//Deduct document values from w
					for(Entry<String, Integer> KVPair : w.entrySet()){
						w.put(KVPair.getKey(), KVPair.getValue() - documentMap.get(KVPair.getKey()));
					}
				}
				
				this.clearValues(documentDictionary);
			}
		}
		
		System.out.print("	Finished training" + System.getProperty("line.separator"));
	}

	public double classify(HashMap<String, Integer> document){
		//System.out.println("Classifyin'");
		//Convert document
		HashMap<String, Integer> d = classifierDictionary;
		//d.putAll(dictionary);
		d.putAll(document);
		
		double tempWeight = 0;
		for(Entry<String, Integer> KVPair : w.entrySet()){
			tempWeight += d.get(KVPair.getKey()) * KVPair.getValue();
		}
		//System.out.println("Classifyed'");
		this.clearValues(classifierDictionary);
		return tempWeight - bias;

		
	}
	
	private void clearValues(HashMap<String, Integer> dirtyMap){
		Set<Entry<String, Integer>> dirtySet = dirtyMap.entrySet();
		for( Entry<String, Integer> KVPair : dirtySet){
			KVPair.setValue(0);
		}
		
	}
	
	public String getCategory(){
		return category;
	}

	
	
	
}
