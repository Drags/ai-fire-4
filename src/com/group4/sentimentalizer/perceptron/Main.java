package com.group4.sentimentalizer.perceptron;


import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import com.group4.sentimentalizer.*;



public class Main {

	static String[] categories = {"books" , "dvd", "camera", "health", "music", "software"};
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		final Wordbag dictionary = new Wordbag();
		File directory;
		int size;
		int counter = 0;
		
		//Reads all the documents, counts the number of occurrences of each word and stores them in a Wordbag
		for(String category : categories){
	
			directory = new File("punctuation/"+ category +"/pos/");
			size = directory.listFiles().length;

			for (File file : directory.listFiles()) {
				if (file.isFile()) {
					dictionary.processFile(file);
				}
				//System.out.println("processed file: "+(++counter)+"/"+size);
			}
			
			directory = new File("punctuation/"+ category +"/neg/");
			size = directory.listFiles().length;
			for (File file : directory.listFiles()) {
				if (file.isFile()) {
					dictionary.processFile(file);
				}
				//System.out.println("processed file: "+(++counter)+"/"+size);
			}
			
		}
		
//		System.out.println("\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING 2-FOLD CATEGORIZATION \u25A1\u25A1\u25A1\u25A1\u25A1");
//		Main.categoryClassifier(dictionary, 2);
//		
//		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING 5-FOLD CATEGORIZATION \u25A1\u25A1\u25A1\u25A1\u25A1");
//		Main.categoryClassifier(dictionary, 5);
		
		
		
//		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING 2-FOLD INDOMAIN BOOKS \u25A1\u25A1\u25A1\u25A1\u25A1");
//		Main.inDomainSentimentilizer(dictionary, "pos", "books", 2);
		
//		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING 2-FOLD INDOMAIN CAMERA \u25A1\u25A1\u25A1\u25A1\u25A1");
//		Main.inDomainSentimentilizer(dictionary, "pos", "camera", 2);
//		
//		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING 2-FOLD INDOMAIN DVD \u25A1\u25A1\u25A1\u25A1\u25A1");
//		Main.inDomainSentimentilizer(dictionary, "pos", "dvd", 2);
//		
//		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING 2-FOLD INDOMAIN HEALTH \u25A1\u25A1\u25A1\u25A1\u25A1");
//		Main.inDomainSentimentilizer(dictionary, "pos", "health", 2);
//		
//		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING 2-FOLD INDOMAIN MUSIC \u25A1\u25A1\u25A1\u25A1\u25A1");
//		Main.inDomainSentimentilizer(dictionary, "pos", "music", 2);
//		
//		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING 2-FOLD INDOMAIN SOFTWARE \u25A1\u25A1\u25A1\u25A1\u25A1");
//		Main.inDomainSentimentilizer(dictionary, "pos", "music", 2);
		
		
		
		
		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING OUT OF DOMAIN BOOKS - CAMERA \u25A1\u25A1\u25A1\u25A1\u25A1");
		Main.outDomainSentimentilizer(dictionary, "pos", "books", "camera");
		
		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING OUT OF DOMAIN BOOKS - DVD \u25A1\u25A1\u25A1\u25A1\u25A1");
		Main.outDomainSentimentilizer(dictionary, "pos", "books", "dvd");
		
		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING OUT OF DOMAIN BOOKS - HEALTH \u25A1\u25A1\u25A1\u25A1\u25A1");
		Main.outDomainSentimentilizer(dictionary, "pos", "books", "dvd");
		
		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING OUT OF DOMAIN BOOKS - MUSIC \u25A1\u25A1\u25A1\u25A1\u25A1");
		Main.outDomainSentimentilizer(dictionary, "pos", "books", "music");
		
		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING OUT OF DOMAIN BOOKS - SOFTWARE \u25A1\u25A1\u25A1\u25A1\u25A1");
		Main.outDomainSentimentilizer(dictionary, "pos", "books", "software");
		
		
		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING OUT OF DOMAIN CAMERA - BOOKS \u25A1\u25A1\u25A1\u25A1\u25A1");
		Main.outDomainSentimentilizer(dictionary, "pos", "camera", "books");
		
		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING OUT OF DOMAIN CAMERA - DVD \u25A1\u25A1\u25A1\u25A1\u25A1");
		Main.outDomainSentimentilizer(dictionary, "pos", "camera", "dvd");
		
		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING OUT OF DOMAIN CAMERA - HEALTH \u25A1\u25A1\u25A1\u25A1\u25A1");
		Main.outDomainSentimentilizer(dictionary, "pos", "camera", "health");
		
		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING OUT OF DOMAIN CAMERA - MUSIC \u25A1\u25A1\u25A1\u25A1\u25A1");
		Main.outDomainSentimentilizer(dictionary, "pos", "camera", "music");
		
		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING OUT OF DOMAIN CAMERA - SOFTWARE \u25A1\u25A1\u25A1\u25A1\u25A1");
		Main.outDomainSentimentilizer(dictionary, "pos", "camera", "software");
		
		
		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING OUT OF DOMAIN DVD - BOOKS \u25A1\u25A1\u25A1\u25A1\u25A1");
		Main.outDomainSentimentilizer(dictionary, "pos", "dvd", "books");
		
		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING OUT OF DOMAIN DVD - CAMERA \u25A1\u25A1\u25A1\u25A1\u25A1");
		Main.outDomainSentimentilizer(dictionary, "pos", "dvd", "camera");
		
		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING OUT OF DOMAIN DVD - HEALTH \u25A1\u25A1\u25A1\u25A1\u25A1");
		Main.outDomainSentimentilizer(dictionary, "pos", "dvd", "health");
		
		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING OUT OF DOMAIN DVD - MUSIC \u25A1\u25A1\u25A1\u25A1\u25A1");
		Main.outDomainSentimentilizer(dictionary, "pos", "dvd", "music");
		
		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING OUT OF DOMAIN DVD - SOFTWARE \u25A1\u25A1\u25A1\u25A1\u25A1");
		Main.outDomainSentimentilizer(dictionary, "pos", "dvd", "software");
		
		
		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING OUT OF DOMAIN HEALTH - BOOKS \u25A1\u25A1\u25A1\u25A1\u25A1");
		Main.outDomainSentimentilizer(dictionary, "pos", "health", "books");
		
		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING OUT OF DOMAIN HEALTH - CAMERA \u25A1\u25A1\u25A1\u25A1\u25A1");
		Main.outDomainSentimentilizer(dictionary, "pos", "health", "camera");
		
		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING OUT OF DOMAIN HEALTH - DVD \u25A1\u25A1\u25A1\u25A1\u25A1");
		Main.outDomainSentimentilizer(dictionary, "pos", "health", "dvd");
		
		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING OUT OF DOMAIN HEALTH - MUSIC \u25A1\u25A1\u25A1\u25A1\u25A1");
		Main.outDomainSentimentilizer(dictionary, "pos", "health", "music");
		
		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING OUT OF DOMAIN HEALTH - SOFTWARE \u25A1\u25A1\u25A1\u25A1\u25A1");
		Main.outDomainSentimentilizer(dictionary, "pos", "health", "software");
		
		
		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING OUT OF DOMAIN MUSIC - BOOKS \u25A1\u25A1\u25A1\u25A1\u25A1");
		Main.outDomainSentimentilizer(dictionary, "pos", "music", "books");	
		
		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING OUT OF DOMAIN MUSIC - CAMERA \u25A1\u25A1\u25A1\u25A1\u25A1");
		Main.outDomainSentimentilizer(dictionary, "pos", "music", "camera");	
		
		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING OUT OF DOMAIN MUSIC - DVD \u25A1\u25A1\u25A1\u25A1\u25A1");
		Main.outDomainSentimentilizer(dictionary, "pos", "music", "dvd");	
		
		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING OUT OF DOMAIN MUSIC - HEALTH \u25A1\u25A1\u25A1\u25A1\u25A1");
		Main.outDomainSentimentilizer(dictionary, "pos", "music", "health");
		
		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING OUT OF DOMAIN MUSIC - SOFTWARE \u25A1\u25A1\u25A1\u25A1\u25A1");
		Main.outDomainSentimentilizer(dictionary, "pos", "music", "software");
		
		
		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING OUT OF DOMAIN SOFTWARE - BOOKS \u25A1\u25A1\u25A1\u25A1\u25A1");
		Main.outDomainSentimentilizer(dictionary, "pos", "software", "books");
		
		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING OUT OF DOMAIN SOFTWARE - CAMERA \u25A1\u25A1\u25A1\u25A1\u25A1");
		Main.outDomainSentimentilizer(dictionary, "pos", "software", "camera");
		
		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING OUT OF DOMAIN SOFTWARE - DVD \u25A1\u25A1\u25A1\u25A1\u25A1");
		Main.outDomainSentimentilizer(dictionary, "pos", "software", "dvd");
		
		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING OUT OF DOMAIN SOFTWARE - HEALTH \u25A1\u25A1\u25A1\u25A1\u25A1");
		Main.outDomainSentimentilizer(dictionary, "pos", "software", "health");
		
		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING OUT OF DOMAIN SOFTWARE - MUSIC \u25A1\u25A1\u25A1\u25A1\u25A1");
		Main.outDomainSentimentilizer(dictionary, "pos", "software", "music");
//		
//		
//		
//		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING 5-FOLD INDOMAIN BOOKS \u25A1\u25A1\u25A1\u25A1\u25A1");
//		Main.inDomainSentimentilizer(dictionary, "pos", "books", 5);
//		
//		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING 5-FOLD INDOMAIN CAMERA \u25A1\u25A1\u25A1\u25A1\u25A1");
//		Main.inDomainSentimentilizer(dictionary, "pos", "camera", 5);
//		
//		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING 5-FOLD INDOMAIN DVD \u25A1\u25A1\u25A1\u25A1\u25A1");
//		Main.inDomainSentimentilizer(dictionary, "pos", "dvd", 5);
//		
//		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING 5-FOLD INDOMAIN HEALTH \u25A1\u25A1\u25A1\u25A1\u25A1");
//		Main.inDomainSentimentilizer(dictionary, "pos", "health", 5);
//		
//		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING 5-FOLD INDOMAIN MUSIC \u25A1\u25A1\u25A1\u25A1\u25A1");
//		Main.inDomainSentimentilizer(dictionary, "pos", "music", 5);
//		
//		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING 5-FOLD INDOMAIN SOFTWARE \u25A1\u25A1\u25A1\u25A1\u25A1");
//		Main.inDomainSentimentilizer(dictionary, "pos", "software", 5);
//		
//		
		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING 10-FOLD CATEGORIZATION \u25A1\u25A1\u25A1\u25A1\u25A1");
		Main.categoryClassifier(dictionary, 10);
		
		
		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING 10-FOLD INDOMAIN BOOKS \u25A1\u25A1\u25A1\u25A1\u25A1");
		Main.inDomainSentimentilizer(dictionary, "pos", "books", 10);
		
		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING 10-FOLD INDOMAIN CAMERA \u25A1\u25A1\u25A1\u25A1\u25A1");
		Main.inDomainSentimentilizer(dictionary, "pos", "camera", 10);
		
		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING 10-FOLD INDOMAIN DVD \u25A1\u25A1\u25A1\u25A1\u25A1");
		Main.inDomainSentimentilizer(dictionary, "pos", "dvd", 10);
		
		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING 10-FOLD INDOMAIN HEALTH \u25A1\u25A1\u25A1\u25A1\u25A1");
		Main.inDomainSentimentilizer(dictionary, "pos", "health", 10);
		
		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING 10-FOLD INDOMAIN MUSIC \u25A1\u25A1\u25A1\u25A1\u25A1");
		Main.inDomainSentimentilizer(dictionary, "pos", "music", 10);
		
		System.out.println("\n\n\u25A1\u25A1\u25A1\u25A1\u25A1 STARTING 10-FOLD INDOMAIN SOFTWARE \u25A1\u25A1\u25A1\u25A1\u25A1");
		Main.inDomainSentimentilizer(dictionary, "pos", "software", 10);
		
		
		
		//Do out-of-domain sentimentalizer
//		for(String cat1 : categories){
//			for(String cat2 : categories){
//				if(!cat1.equals(cat2)){					
//					System.out.println("============Training on " + cat1 + ". Testing on "+cat2+"================");
//					Main.outDomainSentimentilizer(dictionary, "pos", cat1, cat2);
//					System.out.println("=======================================================================");
//				}
//			}
//		}
		
		
		System.out.println("Program finished!");
		
	}
	
	
	public static void categoryClassifier(Wordbag dictionary, int n){
		//Create perceptrons
		List<Perceptron> perceptronList = new ArrayList<Perceptron>();
		Perceptron tempPerceptron;
		for(String s : categories){
			tempPerceptron = new Perceptron(dictionary, s);
			perceptronList.add(tempPerceptron);
		}
		
		//int n = 2; //n-fold cross validation
		int start = 1;
		int end = 1000/n;
		float percentCorrect = 0;
		
		int Totcorrect = 0;
		int TotinconclusiveAllZero = 0;
		int TotinconclusiveMoreThanOneAndPos = 0;
		int TotinconclusiveMoreThanOneAndNeg = 0;
		int TotincorrectPositives = 0;
		int TotnoOfDocuments = 0;
		
		
		for(int tests = 0 ; tests<n ; tests++ ){
			
			int correct = 0;
			int inconclusiveAllZero = 0;
			int incorrectPositives = 0;
			int noOfDocuments = 0;
			
			System.out.println("	----Subtest started with Start: " + start + " and End: " + end + " ---");
			//Train
			long trainTime = System.currentTimeMillis();
			HashMap<HashMap<String, Integer>, String> trainMap = new HashMap<HashMap<String, Integer>, String>();
			if(start != 1){
				trainMap.putAll(Main.getCatSet(1, start-1));
			}
			if(end != 1000){
				trainMap.putAll(Main.getCatSet(end+1, 1000));
			}
			for(Perceptron p : perceptronList){
				p.train(trainMap);
			}
			//String msg = "TrainTime: " + (System.currentTimeMillis() - trainTime);
			
			//Documents to classify
			HashMap<HashMap<String, Integer>, String> docsToClassify = Main.getCatSet(start, end);
			
//			int correct = 0;
//			int incorrect = 0;
			
			for(Entry<HashMap<String, Integer>, String> document : docsToClassify.entrySet()){
				HashMap<String, Double> resultMap = new HashMap<String, Double>();
				int noOfHits = 0;
				//Classify
				for(Perceptron p : perceptronList){
					
					resultMap.put(p.getCategory(),  p.classify(document.getKey()));
				}
				
				for(Entry<String, Double> KVPair: resultMap.entrySet()){
					if(KVPair.getValue() > 0){
						noOfHits++;
					}
				}
				
				String stringHit = "";
				if(noOfHits != 1){
					boolean isHit = false;
					if(noOfHits == 0) {
						inconclusiveAllZero++;
					} else {
						String correctString = document.getValue();
						Entry<String, Double> highestPair = null;
						boolean firstRun = true;
						
						
						for(Entry<String, Double> KVPair: resultMap.entrySet()){
							
							if(KVPair.getValue() > 0){
								if(firstRun){
									firstRun = false;
									highestPair = KVPair;
								} else {
									if(KVPair.getValue() > highestPair.getValue()){
										highestPair = KVPair;
									}
								}
							}
						}
						if(highestPair.getKey().equals(correctString)){
							correct++;
						} else{
							incorrectPositives++;
						}
						
						
					}
				}
				else{
					for(Entry<String, Double> KVPair: resultMap.entrySet()){
						if(KVPair.getValue() > 0){
							stringHit = KVPair.getKey();
						}
					}
					if(document.getValue().equals(stringHit)){
						//System.out.println("Correct classification");
						correct++;
					} else {
						//System.out.println("Incorrect classification: Not Equal");
						incorrectPositives++;
					}
					
				}
				noOfDocuments++;
				
				
			}
			

			System.out.println("	----Subtest ended with Start: " + start + " and End: " + end + " ---");
			System.out.println("	Correct classifications:____________________________________" + correct);
			System.out.println("	Inconclusive: All was zero:_________________________________" + inconclusiveAllZero);
			System.out.println("	False positive:_____________________________________________" + incorrectPositives);
			System.out.println("	Number of Documents:________________________________________" + noOfDocuments);
			System.out.println("	----------------------------------------------------------------");
			
			Totcorrect += correct;
			TotinconclusiveAllZero += inconclusiveAllZero;
			TotincorrectPositives += incorrectPositives;
			TotnoOfDocuments += noOfDocuments;
			
			percentCorrect += correct / (correct+inconclusiveAllZero+incorrectPositives+0.0f);
			
			start += 1000/n;
			end += 1000/n;
		}
		
		System.out.println("TOTAL STATISTICS FOR " + n + "-FOLD CROSS VALIDATION - CATEGORIES");
		System.out.println(">Correct classifications:____________________________________" + Totcorrect);
		System.out.println(">Inconclusive: All was zero:_________________________________" + TotinconclusiveAllZero);
		System.out.println(">Inconclusive: More than one positive including correct:_____" + TotinconclusiveMoreThanOneAndPos);
		System.out.println(">Inconclusive: More than one positive not including correct:_" + TotinconclusiveMoreThanOneAndNeg);
		System.out.println(">False positive:_____________________________________________" + TotincorrectPositives);
		System.out.println(">Number of Documents:________________________________________" + TotnoOfDocuments);
		
		
		percentCorrect = percentCorrect/(n + 0.0f);
		
		System.out.println("Average correctness:________________________________________" + percentCorrect);
		
	}

	public static void inDomainSentimentilizer(Wordbag dictionary, String sentiment, String cat, int n){
		
//		int n = 5; //n-fold cross validation
		int start = 1;
		int end = 1000/n;
		
		int TotcorrectPositive = 0;
		int TotcorrectNegative = 0;
		int TotincorrectPositives = 0;
		int TotincorrectNegatives = 0;
		int TotnoOfDocuments = 0;
		
		for(int tests = 0 ; tests<n ; tests++ ){
			
			int correctPositive = 0;
			int correctNegative = 0;
			int incorrectPositives = 0;
			int incorrectNegatives = 0;
			int noOfDocuments = 0;
			
			//Create perceptron
			Perceptron p = new Perceptron(dictionary, sentiment);
			
			//Train
			long trainTime = System.currentTimeMillis();
			//Read the documents that are before the testing data
			HashMap<HashMap<String, Integer>, String> trainMap = Main.getSentimentSet(0, start-1, cat);
			//Add the documents that is after the testing data
			HashMap<HashMap<String, Integer>, String> docsAfter = Main.getSentimentSet(end+1, 1000, cat);
			trainMap.putAll(docsAfter);

			
			p.train(trainMap);
			String msg = "TrainTime: " + (System.currentTimeMillis() - trainTime);
			
			//Documents to classify
			HashMap<HashMap<String, Integer>, String> docsToClassify = Main.getSentimentSet(start, end, cat);
			
//			int correct = 0;
//			int incorrect = 0;
			
			for(Entry<HashMap<String, Integer>, String> document : docsToClassify.entrySet()){
				
				//Classify
				double result = p.classify(document.getKey());
				
				//"If classified as book and it is book" -> Correct
				if((result > 0) && document.getValue().equals(sentiment)){
					//System.out.println("Correct classification");
					correctPositive++;
				}else if((result <= 0) && (!document.getValue().equals(sentiment))){ //"if classified as something else than book and it isn't book" -> Correct
					//System.out.println("Correct classification");
					correctNegative++;
				}else if((result > 0) && (!document.getValue().equals(sentiment))){ //Incorrect. "Result is book but isn't book and result isn't book but it is book" 
					//System.out.println("Incorrect classification");
					//System.out.println("-------DOCUMENT CLASSIFIED AS "+ result + " WHEN IT IS " + document.getValue() +"----------");
					//printDocument(document.getKey());
					//System.out.println("---------------------------------------------");
				
					incorrectPositives++;
				} else {
					incorrectNegatives++;
				}
				
				noOfDocuments++;
			}
			
			System.out.println("	----Subtest ended with Start: " + start + " and End: " + end + " ---");
			System.out.println("	Correct Positive:____________________________________" + correctPositive);
			System.out.println("	Correct Negative:____________________________________" + correctNegative);
			System.out.println("	False positive:_____________________________________________" + incorrectPositives);
			System.out.println("	False negative:_____________________________________________" + incorrectNegatives);
			System.out.println("	Number of Documents:________________________________________" + noOfDocuments);
			System.out.println("	----------------------------------------------------------------");
			
			TotcorrectPositive += correctPositive;
			TotcorrectNegative += correctNegative;
			TotincorrectPositives += incorrectPositives;
			TotincorrectNegatives += incorrectNegatives;
			TotnoOfDocuments += noOfDocuments;
			
			
			start += 1000/n;
			end += 1000/n;
			
		}
		System.out.println("TOTAL STATISTICS FOR " + n + "-FOLD CROSS VALIDATION - INDOMAIN: " + cat);
		System.out.println(">Correct Positive:____________________________________" + TotcorrectPositive);
		System.out.println(">Correct Negative:____________________________________" + TotcorrectNegative);
		System.out.println(">False positive:_____________________________________________" + TotincorrectPositives);
		System.out.println(">False negative:_____________________________________________" + TotincorrectNegatives);
		System.out.println(">Number of Documents:________________________________________" + TotnoOfDocuments);
		
		//Calculate the average correctness
		float percentCorrect = (TotcorrectPositive + TotcorrectNegative) / (TotcorrectPositive + TotcorrectNegative +TotincorrectPositives + TotincorrectNegatives + 0.0f);
		float precision = TotcorrectPositive / (TotcorrectPositive + TotcorrectNegative + 0.0f);
		float recall = TotcorrectPositive / (TotcorrectPositive + TotincorrectNegatives + 0.0f);
		float f1score = (2*precision*recall)/(precision+recall+0.0f);

		
		System.out.println(">Accuracy:________________________________________" + percentCorrect);
		System.out.println(">Precision:________________________________________" + precision);
		System.out.println(">Recall:________________________________________" + recall);
		System.out.println(">F1-Score:________________________________________" + f1score);

		
	}
	
	public static void outDomainSentimentilizer(Wordbag dictionary, String sentiment, String trainCat, String testCat){
		
		
		//Create perceptron
		Perceptron p = new Perceptron(dictionary, sentiment);
		
		//Train
		long trainTime = System.currentTimeMillis();
		HashMap<HashMap<String, Integer>, String> trainMap = Main.getSentimentSet(1, 1000, trainCat);
		p.train(trainMap);
		String msg = "TrainTime: " + (System.currentTimeMillis() - trainTime);
		
		//Documents to classify
		HashMap<HashMap<String, Integer>, String> docsToClassify = Main.getSentimentSet(1, 1000, testCat);
		
		int TotcorrectPositive = 0;
		int TotcorrectNegative = 0;
		int TotincorrectPositives = 0;
		int TotincorrectNegatives = 0;
		int TotnoOfDocuments = 0;
		
		for(Entry<HashMap<String, Integer>, String> document : docsToClassify.entrySet()){
			
			//Classify
			double result = p.classify(document.getKey());
			
			//"If classified as book and it is book" -> Correct
			if((result > 0) && document.getValue().equals(sentiment)){
				//System.out.println("Correct classification");
				TotcorrectPositive++;
			}else if((result<=0) && (!document.getValue().equals(sentiment))){ //"if classified as something else than book and it isn't book" -> Correct
				//System.out.println("Correct classification");
				TotcorrectNegative++;
			}else if((result>0) && (!document.getValue().equals(sentiment))){ //Incorrect. "Result is book but isn't book and result isn't book but it is book" 

			
				TotincorrectPositives++;
			} else {
				TotincorrectNegatives++;
			}
			
			TotnoOfDocuments++;
			
		}
		
		System.out.println(">Correct Positive:____________________________________" + TotcorrectPositive);
		System.out.println(">Correct Negative:____________________________________" + TotcorrectNegative);
		System.out.println(">False positive:_____________________________________________" + TotincorrectPositives);
		System.out.println(">False negative:_____________________________________________" + TotincorrectNegatives);
		System.out.println(">Number of Documents:________________________________________" + TotnoOfDocuments);
		
		//Calculate the average correctness
		float percentCorrect = (TotcorrectPositive + TotcorrectNegative) / (TotcorrectPositive + TotcorrectNegative +TotincorrectPositives + TotincorrectNegatives + 0.0f);
		float precision = TotcorrectPositive / (TotcorrectPositive + TotcorrectNegative + 0.0f);
		float recall = TotcorrectPositive / (TotcorrectPositive + TotincorrectNegatives + 0.0f);
		float f1score = (2*precision*recall)/(precision+recall+0.0f);

		
		System.out.println(">Accuracy:________________________________________" + percentCorrect);
		System.out.println(">Precision:________________________________________" + precision);
		System.out.println(">Recall:________________________________________" + recall);
		System.out.println(">F1-Score:________________________________________" + f1score);
	
		
	}
	
	
	public static void printDocument(HashMap<String, Integer> doc){
		
		for(Entry<String, Integer> word : doc.entrySet()){
			System.out.println("Word: " + word.getKey() + " " + word.getValue());
		}
		
		
	}
	
	/**
	 * Method that returns a map with document as key and correct category as value 
	 * @param start
	 * @param length
	 * @return 
	 */
	public static HashMap<HashMap<String, Integer>, String> getCatSet(int start, int length){
		HashMap<HashMap<String, Integer>, String> returnMap = new HashMap<HashMap<String, Integer>, String>();
		
		//Only get files from the start value to end value (e.g. 100.txt to 200.txt for each category)
//		System.out.println("	getting CatSet");
		for(; start <= length; start++){
			
			
			//Loop through all categories
			for(String category : Main.categories){
				
				//Get all positive reviews in the current category
				Wordbag first = new Wordbag();
				File file;
				String dirString = "punctuation/"+ category +"/pos/" + start +".txt";
				file = new File(dirString);
				if (file.isFile()) {
					first.processFile(file);
				}
				returnMap.put(first.getMap(), category);
				
				//Get all negative reviews in the current cat
				first = new Wordbag();
				file = new File("punctuation/"+ category +"/neg/" + start + ".txt");
				if (file.isFile()) {
					first.processFile(file);
				}
				returnMap.put(first.getMap(), category);
				
			}	
		}
		return returnMap;
	}
	
	/**
	 * Method that returns a map with document as key and "pos" or "neg"  as value 
	 * @param start start at [start].txt
	 * @param length end at [length].txt
	 * @param cat The category (books, dvd, etc..)
	 * @return 
	 */
	public static HashMap<HashMap<String, Integer>, String> getSentimentSet(int start, int length, String cat){
		HashMap<HashMap<String, Integer>, String> returnMap = new HashMap<HashMap<String, Integer>, String>();
		
		//System.out.println("Getting Sentiment Set");
		
		//Only get files from the start value to end value (e.g. 100.txt to 200.txt for the specified category)
		for(; start <= length; start++){
			//Get all positive reviews in the current category
			Wordbag first = new Wordbag();
			File file;
			String dirString = "punctuation/"+ cat +"/pos/" + start +".txt";
			file = new File(dirString);
			if (file.isFile()) {
				first.processFile(file);
			}
			returnMap.put(first.getMap(), "pos");
			
			//Get all negative reviews in the current cat
			first = new Wordbag();
			file = new File("punctuation/"+ cat +"/neg/" + start + ".txt");
			if (file.isFile()) {
				first.processFile(file);
			}
			returnMap.put(first.getMap(), "neg");
				
		}
		return returnMap;
	}
	
}
