package com.group4.sentimentalizer.perceptron;

import java.util.HashMap;
import java.util.Set;
import java.util.Map.Entry;

import com.group4.sentimentalizer.Wordbag;

/**
 * Averaged Perceptron
 * 
 * @author johan & simon
 */

public class AvgPerceptron {

	HashMap<String, Integer> classifierDictionary;
	HashMap<String, Integer> documentDictionary;
	HashMap<String, Float> wAvg;
	HashMap<String, Integer> w;
	float bias = 0;
	float learningRate = 0.5f;
	long lastTime = System.nanoTime();
	//books, dvd, pos, neg, etc..
	String category;
	
	
	public AvgPerceptron(Wordbag wordbag, String category){
		classifierDictionary = new HashMap<String, Integer>();
		classifierDictionary.putAll(wordbag.getMap());
		this.clearValues(classifierDictionary);
		
		documentDictionary = new HashMap<String, Integer>();
		documentDictionary.putAll(classifierDictionary);
		
		wAvg = new HashMap<String, Float>();
		w = new HashMap<String, Integer>();
		
		this.category = category;
		
		
	}
	
	public void train(HashMap<HashMap<String, Integer>, String> trainMap){
		System.out.print("Started training. ");
		
		
		//Clear the weights
		w.putAll(classifierDictionary);
		//Clear average weights and set value to float
		for(Entry<String, Integer> word : classifierDictionary.entrySet())
			wAvg.put(word.getKey(), word.getValue().floatValue());
		
		final int iterations = 3;
		final int t = trainMap.size(); //Size of training set

		System.out.print("Training iteration: ");
		for(int i = 0; i < iterations; i++){
			System.out.print(i + "..");
			//Iterate over the entire set of documents.
			for(Entry<HashMap<String, Integer>, String> trainEntry : trainMap.entrySet()){
				//Put current document into the complete dictionary.
				HashMap<String, Integer> documentMap = documentDictionary;

				documentMap.putAll(trainEntry.getKey());

				//Do trainClassify so that it classifies using the normal (not averaged) weights
				double result = trainClassify(trainEntry.getKey());
				boolean expected = trainEntry.getValue().equals(category);

				//If maths then maths
				if((result > 0) == expected){
					//Do nothing
				} else if ((result < 0) && expected){ //If result false and expected true
					//Decrease bias with learning rate
					bias -= learningRate;
					
					float averageWeight = ((iterations*t) -i) / (iterations*t); //(N*T-C)/(N*T) in the example on the course webpage
					//Add values of documents to w
					for(Entry<String, Integer> KVPair : w.entrySet()){
						w.put(KVPair.getKey(), KVPair.getValue() + documentMap.get(KVPair.getKey()));
						
						//calculate average weights (wAvg + averageWeight*frequency of word)
						wAvg.put(KVPair.getKey(), wAvg.get(KVPair.getKey()) + (averageWeight*documentMap.get(KVPair.getKey())));
					}

					
					
					
				} else {//If result true and expected false
					//Increase bias with learning rate
					bias += learningRate;
					
					float averageWeight = ((iterations*t) -i) / (iterations*t); //(N*T-C)/(N*T) in the example on the course webpage
					//Deduct document values from w
					for(Entry<String, Integer> KVPair : w.entrySet()){
						w.put(KVPair.getKey(), KVPair.getValue() - documentMap.get(KVPair.getKey()));
						
						//calculate average weights. (wAvg - averageWeight*frequency of word)
						wAvg.put(KVPair.getKey(), wAvg.get(KVPair.getKey()) - (averageWeight*documentMap.get(KVPair.getKey())));
					}
				}
				
				this.clearValues(documentDictionary);
			}
		}
		
		System.out.print(" Finished training" + System.getProperty("line.separator"));
	}
	
	/**
	 * Classifier used when training. Uses the normal weights instead of avg weights
	 * @param document
	 * @return
	 */
	private double trainClassify(HashMap<String, Integer> document){
		//Convert document
		HashMap<String, Integer> d = classifierDictionary;
		d.putAll(document);
		
		double tempWeight = 0;
		for(Entry<String, Integer> KVPair : w.entrySet()){
			tempWeight += d.get(KVPair.getKey()) * KVPair.getValue();
		}
		this.clearValues(classifierDictionary);
		return tempWeight - bias;
	}

	/**
	 * Classifier using the average weights
	 * @param document
	 * @return
	 */
	public double classify(HashMap<String, Integer> document){
		//Convert document
		HashMap<String, Integer> d = classifierDictionary;
		d.putAll(document);
		
		double tempWeight = 0;
		for(Entry<String, Float> KVPair : wAvg.entrySet()){
			tempWeight += d.get(KVPair.getKey()) * KVPair.getValue();
		}
		this.clearValues(classifierDictionary);
		return tempWeight - bias;
		
	}
	
	private void clearValues(HashMap<String, Integer> dirtyMap){
		Set<Entry<String, Integer>> dirtySet = dirtyMap.entrySet();
		for( Entry<String, Integer> KVPair : dirtySet){
			KVPair.setValue(0);
		}
		
	}
	
	public String getCategory(){
		return category;
	}

	
	
	
	
}
